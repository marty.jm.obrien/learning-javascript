//Exercise 1

var pyramid = "#";
for(i = 1; i <= 7; i++) {
    console.log(pyramid);
    pyramid += "#";
};

//Exercise 2

for( i = 1; i <= 100; i++) {
    if( i % 3 === 0 && i % 5 === 0 ) {
        console.log("FizzBuzz");
    } else if(i % 3 === 0) {
            console.log("Fizz");
    } else if( i % 5 === 0 ) {
        console.log("Buzz");
    } else {
        console.log(i);
    };
};

//Exercise 2.1

var userChoice = prompt("Pick a fizzing number!");
for( i = 1; i <= userChoice; i++) {
    if( i % 3 === 0 && i % 5 === 0 ) {
        console.log("FizzBuzz");
    } else if(i % 3 === 0) {
            console.log("Fizz");
    } else if( i % 5 === 0 ) {
        console.log("Buzz");
    } else {
        console.log(i);
    };
};

//Exercise 3

var startChar = "#";
var chessBoard = "";
var lines = 0;
while( lines < 8 ) {
    if( lines % 2 === 0 ) {
        for( i = 0; i < 8; i++) {
            if( startChar === "#" ) {
                chessBoard += " ";
                startChar = " ";
            } else if ( startChar === " " ) {
                chessBoard += "#";
                startChar = "#";
            };
        };
    } else {
        for( i = 0; i < 8; i++) {
            if( startChar === "#" ) {
                chessBoard += "#";
                startChar = " ";
            } else if ( startChar === " " ) {
                chessBoard += " ";
                startChar = "#";
            }; 
        };
    };
    chessBoard += "\n";
    lines += 1;
};

console.log(chessBoard);

//Exercise 3.1

var userChoice = prompt("Please choose your chess board size");
var startChar = "#";
var chessBoard = "";
var lines = 0;
while( lines < userChoice ) {
    if( lines % 2 === 0 ) {
        for( i = 0; i < userChoice; i++) {
            if( startChar === "#" ) {
                chessBoard += " ";
                startChar = " ";
            } else if ( startChar === " " ) {
                chessBoard += "#";
                startChar = "#";
            };
        };
    } else {
        for( i = 0; i < userChoice; i++) {
            if( startChar === "#" ) {
                chessBoard += "#";
                startChar = " ";
            } else if ( startChar === " " ) {
                chessBoard += " ";
                startChar = "#";
            }; 
        };
    };
    chessBoard += "\n";
    if( userChoice % 2 !== 0 ) {
        if( startChar === "#") {
            startChar = " ";
        } else {
            startChar = "#";
        };
    };
    lines += 1;
};

console.log(chessBoard);

//Exercise 4

function min(val1, val2) {
    if ( val1 > val2 ) {
        console.log(val1);
    } else {
        console.log(val2);
    };
};

min(5, 15);

//Exercise 5

function isEven(num) {
    if (num >= 0) {
        if (num === 0) {
            return true;
        } else if (num === 1) {
            return false;
        } else {
            return isEven(num - 2);
        };
    } else {
        return "Please enter a number greater than zero.";
    }
};

isEven(97);

//Exercise 6

function countBs(string) {
    var count = 0;
    for ( i = 0; i <= string.length; i++) {
        if ( string.charAt(i) === "B" ) {
            count++;
        };
    };
    return count;
};

countBs("Baby Baby Baby");

//Exercise 7

function countChar(string, char) {
    var count = 0;
    for ( i = 0; i <= string.length; i++) {
        if ( string.charAt(i) === char ) {
            count++;
        };
    };
    return count;
};

countChar("Baby Baby Baby tullip", "a");

//Exercise 8

// function range(start, end) {
//     var rangeArray = [];
//     for( i = start; i <= end; i++ ) {
//         rangeArray.push(i);
//     }
//     return rangeArray;
// };

function range(start, end, step) {
    var rangeArray = [];
    if( start < end ) {
        for( i = start; i <= end; ) {
            rangeArray.push(i);
            if( step === undefined ) {
                i++;
            } else {
                i += step;
            };
        };
    } else if( start > end ) {
        for( i = start; i >= end; ) {
            rangeArray.push(i);
            if( step === undefined ) {
                i--;
            } else {
                i += step;
            };
        };
    };
    return rangeArray;
};

function sum(arrayToSum) {
    var arraySum = 0;
    for( i = 0; i < arrayToSum.length; i++ ) {
        arraySum += arrayToSum[i];
    };
    return arraySum;
};

console.log(sum(range(50, 1, -2)));